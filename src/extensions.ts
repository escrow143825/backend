import { Numeric, formatEther, formatUnits, parseEther, parseUnits } from "ethers";

/**
 * Parses the current string as an Ether value.
 *
 * @return {bigint} The parsed Ether value.
 */
declare global {
  interface String {
    parseEther(): bigint;
    formatEther(): string;
    parseUnits(unit?: string | Numeric): bigint;
    formatUnits(unit?: string | Numeric): string;
    bigint(): bigint;
  }

  interface Number {
    parseEther(): bigint;
    formatEther(): string;
    parseUnits(unit?: string | Numeric): bigint;
    formatUnits(unit?: string | Numeric): string;
    bigint(): bigint;
  }
}

String.prototype.parseEther = function () {
  return parseEther(this.toString());
};

String.prototype.formatEther = function () {
  return formatEther(this.toString());
};

String.prototype.parseUnits = function (unit?: string | Numeric) {
  return parseUnits(this.toString(), unit);
};

String.prototype.formatUnits = function (unit?: string | Numeric) {
  return formatUnits(this.toString(), unit);
};

String.prototype.bigint = function () {
  return BigInt(this.toString());
};

Number.prototype.parseEther = function () {
  return parseEther(this.toString());
};

Number.prototype.formatEther = function () {
  return formatEther(this.toString());
};

Number.prototype.parseUnits = function (unit?: string | Numeric) {
  return parseUnits(this.toString(), unit);
};

Number.prototype.formatUnits = function (unit?: string | Numeric) {
  return formatUnits(this.toString(), unit);
};

Number.prototype.bigint = function () {
  return BigInt(this.toString());
};

export {};
