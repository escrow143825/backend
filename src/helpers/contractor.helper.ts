import crypto from "crypto-js";

const getCheckSum = (data: any) => crypto.SHA256(JSON.stringify(data)).toString(crypto.enc.Hex);

const addCheckSum = (data: any) => {
  const checksum = getCheckSum(data);
  return { ...data, checksum };
};

export const contractor = {
  getCheckSum,
  addCheckSum,
};
