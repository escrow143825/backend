import { ClassConstructor, plainToInstance } from "class-transformer";
import { validateSync } from "class-validator";
import createHttpError from "http-errors";

export interface IConvert<T, V> {
  (cls: ClassConstructor<T>, plain: V): T;
  (cls: ClassConstructor<T>, plain: V[]): T[];
}

const toInstanceSafe: IConvert<any, any> = <T, V>(cls: ClassConstructor<T>, plain: V | V[]) => {
  return plainToInstance(cls, plain);
};

const toInstanceUnsafe = function <T, V>(cls: ClassConstructor<T>, plain: V | V[]) {
  return plainToInstance(cls, plain, { ignoreDecorators: true });
};

const validateRequestBody = <T>(cls: ClassConstructor<T>, req: T): T => {
  const validatedObject = plainToInstance(cls, req);
  const errors = validateSync(validatedObject as any)
    .map((error) => Object.values(error.constraints!))
    .flat();
  if (errors.length) throw new createHttpError[400](errors as any);
  return validatedObject;
};

const validateRequestArrayBody = <T>(cls: ClassConstructor<T>, req: T[]): T[] => {
  const validatedObject = plainToInstance(cls, req);
  const errors = validateSync(validatedObject as any, { whitelist: true })
    .map((error) => Object.values(error.constraints!))
    .flat();
  if (errors.length) throw createHttpError[400](errors as any);
  return validatedObject;
};

export const transformer = {
  toInstanceSafe,
  toInstanceUnsafe,
  validateRequestBody,
  validateRequestArrayBody,
};
