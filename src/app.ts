require("dotenv").config();
import cors from "cors";
import express, { Application } from "express";
import blockchainRoutes from "./api/blockchain/blockchain.route";
import { loggerMiddleware } from "./middleware/logger.middeware";
import { passportMiddleware } from "./middleware/passport.middleware";
import { errorMiddleware } from "./middleware/error.middleware";
import "./extensions";

const app: Application = express();

app.use(cors());
app.use(express.json());

/**
 * Middleware routes
 */
app.use(loggerMiddleware);
app.use(passportMiddleware);

/**
 * API routes
 */
app.use("/blockchain", blockchainRoutes);

/**
 * Error middlewares
 */
app.use(errorMiddleware);

app.listen(process.env.PORT, () => {
  console.log(`Server running on port ${process.env.PORT}`);
});

export default app;
