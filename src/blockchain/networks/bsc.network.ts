import {
  AddressLike,
  BigNumberish,
  ContractTransactionReceipt,
  ContractTransactionResponse,
  MaxUint256,
  Signature,
  SigningKey,
  TypedDataDomain,
  TypedDataField,
  Wallet,
  formatEther,
  verifyTypedData,
} from "ethers";
import { EscrowV3, EscrowV3__factory, TokenV3, TokenV3__factory } from "../typechain-types";
import { DateTime } from "luxon";
import { InternalServerError } from "http-errors";
import { INetwork } from "../network.interface";
import { userCollection } from "../../database";
import { BadRequest } from "http-errors";

export type Signer = Wallet | string | SigningKey;

export class BscNetwork extends INetwork<TokenV3, EscrowV3> {
  constructor(tokenAddress: string, contractAddress: string, rpc: string) {
    super(TokenV3__factory.connect(tokenAddress), EscrowV3__factory.connect(contractAddress), rpc);
    //
    // this.contract.on(this.contract.getEvent("Deposit"), async (address, amount, at, event) => {
    //   // const receipt = await event.getTransactionReceipt();
    //   const txid = receipt.hash;
    //   const from = receipt.from;
    //   const to = receipt.to;
    //   const timestamp = DateTime.now().toMillis();
    //   const token = await this.tokenAddress();
    //   const status = receipt.status ? "success" : "fail";
    //   console.log(`create user txid: ${receipt.hash}, status: ${status}`);

    //   transactionCollection.upsert({
    //     txid,
    //     from,
    //     to,
    //     amount: amount.toString(),
    //     timestamp,
    //     token,
    //     status,
    //   });
    // });
  }

  async getUser(wallet: AddressLike) {
    return this.contract.getUser(wallet);
  }

  async createUser(wallet: AddressLike, isMerchant: boolean) {
    const tx = await this.contract.createUser(wallet, isMerchant);
    const txReceipt = await tx.wait();
    if (!txReceipt || txReceipt.status !== 1) throw new InternalServerError("create user fail");
    return txReceipt;
  }

  async signature(owner: Wallet, spender: string, value: bigint, expiredAt?: number) {
    const nonce = await this.token.nonces(this.getSigner(owner));
    const deadline = expiredAt || DateTime.now().plus({ minutes: 2 }).toMillis();

    const domain: TypedDataDomain = {
      name: await this.tokenName(),
      version: "1",
      chainId: await this.chainId(),
      verifyingContract: await this.tokenAddress(),
    };
    const types: Record<string, Array<TypedDataField>> = {
      Permit: [
        { name: "owner", type: "address" },
        { name: "spender", type: "address" },
        { name: "value", type: "uint256" },
        { name: "nonce", type: "uint256" },
        { name: "deadline", type: "uint256" },
      ],
    };
    const values: Record<string, any> = { owner: owner.address, spender, value, nonce, deadline };
    const signature = await owner.signTypedData(domain, types, values);
    verifyTypedData(domain, types, values, signature);
    return { signature, deadline, domain, types, values, nonce };
  }

  async depositEstimateGas(owner: Signer, amount: BigNumberish): Promise<bigint>;
  async depositEstimateGas(owner: Signer, amount: BigNumberish, options: { toEther: true }): Promise<string>;
  async depositEstimateGas(owner: Signer, amount: BigNumberish, options: { toEther: false }): Promise<bigint>;
  async depositEstimateGas(owner: Signer, amount: BigNumberish, options: { toEther: boolean } = { toEther: false }): Promise<string | bigint> {
    const { fee, gas } = await this.estimateTxFee(() => this.contract.connect(this.getSigner(owner)).deposit.estimateGas(amount));
    return options.toEther ? formatEther(fee) : fee;
  }
  async deposit(signer: Signer, amount: BigNumberish): Promise<ContractTransactionReceipt>;
  async deposit(signer: Signer, amount: BigNumberish, option: { wait: true }): Promise<ContractTransactionReceipt>;
  async deposit(signer: Signer, amount: BigNumberish, option: { wait: false }): Promise<ContractTransactionResponse>;
  async deposit(signer: Signer, amount: BigNumberish, option: { wait: boolean } = { wait: true }) {
    const owner = this.getSigner(signer);
    const depositTx = await this.contract.connect(owner).deposit(amount);
    if (!option.wait) return depositTx;
    const depositReceipt = await depositTx.wait();
    if (!depositReceipt || depositReceipt.status !== 1) throw new InternalServerError("deposit fail");
    return depositReceipt;
  }

  async approveEstimateGas(owner: Signer, spender: AddressLike, amount: BigNumberish): Promise<bigint>;
  async approveEstimateGas(owner: Signer, spender: AddressLike, amount: BigNumberish, option: { toEther: false }): Promise<bigint>;
  async approveEstimateGas(owner: Signer, spender: AddressLike, amount: BigNumberish, option: { toEther: true }): Promise<string>;
  async approveEstimateGas(owner: Signer, spender: AddressLike, amount: BigNumberish, option: { toEther: boolean } = { toEther: false }) {
    const { fee } = await this.estimateTxFee(() => this.token.connect(this.getSigner(owner)).approve.estimateGas(spender, amount));
    return option.toEther ? formatEther(fee) : fee;
  }

  async stake(signer: Signer, amount: BigNumberish): Promise<ContractTransactionReceipt>;
  async stake(signer: Signer, amount: BigNumberish, option: { wait: true }): Promise<ContractTransactionReceipt>;
  async stake(signer: Signer, amount: BigNumberish, option: { wait: false }): Promise<ContractTransactionResponse>;
  async stake(signer: Signer, amount: BigNumberish, option: { wait: boolean } = { wait: true }) {
    const owner = this.getSigner(signer);
    const stakeTx = await this.contract.connect(owner).stake(amount);
    if (!option.wait) return stakeTx;
    const stakeReceipt = await stakeTx.wait();
    if (!stakeReceipt || stakeReceipt.status !== 1) throw new InternalServerError("stake fail");
    return stakeReceipt;
  }

  async unstake(signer: Signer, amount: BigNumberish): Promise<ContractTransactionReceipt>;
  async unstake(signer: Signer, amount: BigNumberish, option: { wait: true }): Promise<ContractTransactionReceipt>;
  async unstake(signer: Signer, amount: BigNumberish, option: { wait: false }): Promise<ContractTransactionResponse>;
  async unstake(signer: Signer, amount: BigNumberish, option: { wait: boolean } = { wait: true }) {
    const owner = this.getSigner(signer);
    const unstakeTx = await this.contract.connect(owner).unstake(amount);
    if (!option.wait) return unstakeTx;
    const unstakeReceipt = await unstakeTx.wait();
    if (!unstakeReceipt || unstakeReceipt.status !== 1) throw new InternalServerError("unstake fail");
    return unstakeReceipt;
  }

  async withdraw(to: string, amount: BigNumberish): Promise<ContractTransactionReceipt>;
  async withdraw(to: string, amount: BigNumberish, option: { wait: true }): Promise<ContractTransactionReceipt>;
  async withdraw(to: string, amount: BigNumberish, option: { wait: false }): Promise<ContractTransactionResponse>;
  async withdraw(to: string, amount: BigNumberish, option: { wait: boolean } = { wait: true }) {
    const tx = await this.contract.withdraw(to, amount);
    if (!option.wait) return tx;
    const txReceipt = await tx.wait();
    if (!txReceipt || txReceipt.status !== 1) throw new InternalServerError("withdraw fail");
    return txReceipt;
  }

  async validateUser(wallet: string) {
    const user = await userCollection.getUserByWallet(wallet);
    if (!(await this.isExistUser(wallet))) {
      await this.createUser(user.wallet, user.isMerchant);
    }
    return user;
  }

  async isExistUser(wallet: string) {
    const user = await this.getUser(wallet);
    return user[1] != BigInt(0);
  }

  async executePermit(owner: Wallet, spender: string) {
    const value = MaxUint256;
    const { signature, deadline } = await this.signature(owner, spender, value);
    const { r, s, v } = Signature.from(signature);
    const { fee, gas } = await this.estimateTxFee(() => this.token.connect(owner).permit.estimateGas(owner, spender, value, deadline, v, r, s));
    const balance = await this.getBalance(owner);
    if (balance < fee) await this.sendNativeTokenFromAdmin(owner, fee - balance);
    const permitTx = await this.token.connect(owner).permit(owner, spender, value, deadline, v, r, s, {
      gasPrice: await this.gasPrice(),
      gasLimit: gas,
    });
    const permitReceipt = await permitTx.wait();
    if (!permitReceipt || permitReceipt.status !== 1) throw new BadRequest("permit failed");
  }
  async executeDeposit(
    owner: Wallet,
    amount: BigNumberish,
  ): Promise<{
    transactionResponse: ContractTransactionResponse;
    transactionReceipt: ContractTransactionReceipt | null;
  }>;
  async executeDeposit(
    owner: Wallet,
    amount: BigNumberish,
    options: { wait: true },
  ): Promise<{
    transactionResponse: ContractTransactionResponse;
    transactionReceipt: ContractTransactionReceipt | null;
  }>;
  async executeDeposit(owner: Wallet, amount: BigNumberish, options: { wait: false }): Promise<ContractTransactionResponse>;
  async executeDeposit(owner: Wallet, amount: BigNumberish, options: { wait: boolean } = { wait: true }) {
    const { fee } = await this.estimateTxFee(() => this.contract.connect(owner).deposit.estimateGas(amount));
    const bnbBalance = await this.getBalance(owner);
    if (fee > bnbBalance) await this.sendNativeTokenFromAdmin(owner, fee - bnbBalance);
    const depositTx = await this.contract.connect(owner).deposit(amount);
    if (!options.wait) return depositTx;
    return {
      transactionResponse: depositTx,
      transactionReceipt: options.wait ? await depositTx.wait() : undefined,
    };
  }

  async executeWithdraw(owner: Wallet, to: string, amount: BigNumberish) {
    const { fee } = await this.estimateTxFee(() => this.contract.connect(owner).withdraw.estimateGas(to, amount));
    const bnbBalance = await this.getBalance(owner);
    if (fee > bnbBalance) await this.sendNativeTokenFromAdmin(owner, fee - bnbBalance);
    const withdrawTx = await this.contract.connect(owner).withdraw(to, amount);
    return withdrawTx.wait();
  }

  async executeStake(owner: Wallet, amount: BigNumberish) {
    const { fee } = await this.estimateTxFee(() => this.contract.connect(owner).stake.estimateGas(amount));
    const bnbBalance = await this.getBalance(owner);
    if (fee > bnbBalance) await this.sendNativeTokenFromAdmin(owner, fee - bnbBalance);
    const stakeTx = await this.contract.connect(owner).stake(amount);
    return stakeTx.wait();
  }

  async executeUnstake(owner: Wallet, amount: BigNumberish) {
    const { fee } = await this.estimateTxFee(() => this.contract.connect(owner).unstake.estimateGas(amount));
    const bnbBalance = await this.getBalance(owner);
    if (fee > bnbBalance) await this.sendNativeTokenFromAdmin(owner, fee - bnbBalance);
    const unstakeTx = await this.contract.connect(owner).unstake(amount);
    return unstakeTx.wait();
  }
}
