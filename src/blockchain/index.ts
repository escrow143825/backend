import { Provider, SigningKey, Wallet } from "ethers";
import { BscNetwork } from "./networks/bsc.network";
import { BadRequest } from "http-errors";
import { NetworkSupport } from "@prisma/client";

export const NETWORK_SUPPORTS: NetworkSupport[] = ["bsc_testnet", "trc_testnet"];

export const ADMIN_PRIVATE_KEY = process.env.ADMIN_PRIVATE_KEY as string;
export const BSC_TESTNET_RPC = process.env.BSC_TESTNET_RPC as string;
export const BSC_TOKEN_ADDRESS = process.env.BSC_TOKEN_ADDRESS as string;
export const BSC_ESCROW_ADDRESS = process.env.BSC_ESCROW_ADDRESS as string;

const BSC_TESTNET_NETWORK = new BscNetwork(BSC_TOKEN_ADDRESS, BSC_ESCROW_ADDRESS, BSC_TESTNET_RPC);

export class INetworkProvider {
  static getAdminSigner = (provider: Provider): Wallet => {
    return new Wallet(ADMIN_PRIVATE_KEY, provider);
  };

  static getSigner = (privateKey: string | SigningKey, provider: Provider): Wallet => {
    return new Wallet(privateKey, provider);
  };

  static getNetwork(network: NetworkSupport) {
    switch (network) {
      case "bsc_testnet":
        return BSC_TESTNET_NETWORK;
      case "trc_testnet":
        throw new BadRequest("");
    }
  }
}
