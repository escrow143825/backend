import { AddressLike, JsonRpcProvider, Provider, Wallet } from "ethers";

export class IProvider {
  provider: Provider;
  private _chaidId?: bigint;

  constructor(public rpc: string) {
    this.provider = new JsonRpcProvider(rpc);
    this.chainId();
  }

  async chainId() {
    if (this._chaidId) return this._chaidId;
    this._chaidId = (await this.provider.getNetwork()).chainId;
    return this._chaidId;
  }

  async gasPrice() {
    return (await this.provider.getFeeData()).gasPrice;
  }

  getCurrentBlock() {
    return this.provider.getBlockNumber();
  }

  getTransactionCount(address: string) {
    return this.provider.getTransactionCount(address);
  }

  getBalance(address: AddressLike) {
    return this.provider.getBalance(address);
  }
}
