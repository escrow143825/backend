import { AddressLike, BaseContract, BigNumberish, Wallet } from "ethers";
import { ADMIN_PRIVATE_KEY } from ".";
import { ERC20Upgradeable } from "./typechain-types";
import { IProvider } from "./provider";
import { PaymentRequired, InternalServerError } from "http-errors";
import { Signer } from "./networks/bsc.network";

interface BlockRange {
  fromBlock?: number;
  toBlock?: number;
}

export class INetwork<T extends ERC20Upgradeable, C extends BaseContract> extends IProvider {
  private _tokenAddress?: string;
  private _tokenName?: string;
  private _tokenSymbol?: string;
  private _tokenDecimals?: bigint;
  private _contractAddress?: string;

  constructor(
    public token: T,
    public contract: C,
    public rpc: string,
  ) {
    super(rpc);
    this.token = token.connect(this.adminSigner) as T;
    this.contract = contract.connect(this.adminSigner) as C;
    this.tokenAddress();
    this.tokenName();
    this.tokenSymbol();
    this.tokenDecimals();
    this.contractAddress();
  }

  get adminSigner() {
    return new Wallet(ADMIN_PRIVATE_KEY, this.provider);
  }

  getSigner(signer: Signer) {
    return signer instanceof Wallet ? signer : new Wallet(signer, this.provider);
  }

  balanceOf(owner: AddressLike) {
    return this.token.balanceOf(owner);
  }

  allowance(owner: AddressLike, spender: AddressLike) {
    return this.token.allowance(owner, spender);
  }

  async approve(owner: Signer, spender: AddressLike, amount: BigNumberish) {
    const approveTx = await this.token.connect(this.getSigner(owner)).approve(spender, amount);
    const approveReceipt = await approveTx.wait();
    return approveReceipt;
  }

  async getTransferTx(from?: AddressLike, to?: AddressLike, amount?: BigNumberish, range?: BlockRange) {
    const _range = range ? await this.buildRange(range) : await this.getBlockRange();
    return await this.token.queryFilter(this.token.filters.Transfer(from, to, amount), _range.fromBlock, _range.toBlock);
  }

  async buildRange(range: BlockRange): Promise<BlockRange> {
    const to = range.toBlock ?? (await this.provider.getBlockNumber());
    const from = range.fromBlock ?? to - 300;
    return { fromBlock: from, toBlock: to };
  }

  // default get 300 lastest block
  // max range 1000
  async getBlockRange(range: number = 300): Promise<BlockRange> {
    const currentBlock = await this.provider.getBlockNumber();
    const _range = Math.min(range, 1000);
    const fromBlock = currentBlock - _range;
    return { fromBlock: fromBlock, toBlock: currentBlock };
  }

  async tokenAddress({ force = true }: { force?: boolean } = {}) {
    if (force || !this._tokenAddress) {
      this._tokenAddress = await this.token.getAddress();
    }
    return this._tokenAddress;
  }

  async tokenName({ force = false }: { force?: boolean } = {}) {
    if (force || !this._tokenName) {
      this._tokenName = await this.token.name();
    }
    return this._tokenName;
  }

  async tokenSymbol({ force = false }: { force?: boolean } = {}) {
    if (force || !this._tokenSymbol) {
      this._tokenSymbol = await this.token.symbol();
    }
    return this._tokenSymbol;
  }

  async tokenDecimals({ force = false }: { force?: boolean } = {}) {
    if (force || !this._tokenDecimals) {
      this._tokenDecimals = await this.token.decimals();
    }
    return this._tokenDecimals;
  }

  async contractAddress({ force = false }: { force?: boolean } = {}) {
    if (force || !this._contractAddress) {
      this._contractAddress = await this.contract.getAddress();
    }
    return this._contractAddress;
  }

  async estimateTxFee(fn: () => Promise<bigint>) {
    const gasPrice = await this.gasPrice();
    if (!gasPrice) throw new PaymentRequired("no gas price");
    const gas = await fn();
    if (gas == BigInt(0)) throw InternalServerError("Gas estimate returned zero");
    return { fee: gas * gasPrice, gas };
  }

  async sendNativeToken(from: Signer, to: AddressLike, amount: bigint) {
    const fromSigner = this.getSigner(from);
    const transferTx = await fromSigner.sendTransaction({
      to: to,
      value: amount,
    });
    const transferReceipt = await transferTx.wait();
    if (!transferReceipt || transferReceipt.status !== 1) throw new InternalServerError("send token failed");
    return transferReceipt;
  }

  async sendNativeTokenFromAdmin(to: AddressLike, amount: bigint) {
    return await this.sendNativeToken(this.adminSigner, to, amount);
  }
}
