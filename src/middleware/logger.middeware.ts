import { NextFunction, Request, RequestHandler, Response } from "express";
import chalk from "chalk";

export const loggerMiddleware: RequestHandler = (
  req: Request,
  res: Response,
  next: NextFunction,
) => {
  console.log("Request to: " + chalk.green(`${req.method} ${req.originalUrl}`));
  next();
};
