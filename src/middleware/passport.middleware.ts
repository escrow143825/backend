import passport from "passport";
import { Strategy as JwtStrategy, ExtractJwt, StrategyOptionsWithSecret } from "passport-jwt";
import { Strategy as LocalStrategy, IStrategyOptions } from "passport-local";
import { RequestHandler } from "express";

interface JwtPayload {
  id: string;
  username: string;
}
export const passportMiddleware: RequestHandler = (req, res, next) => {
  const initJwtPassport = () => {
    const config: StrategyOptionsWithSecret = {
      secretOrKey: process.env.JWT_SECRET_KEY as string,
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    };

    passport.use(
      new JwtStrategy(config, (payload: JwtPayload, done) =>
        payload.id ? done(null, payload) : done(null, false),
      ),
    );
  };
  const initLocalPassport = () => {
    const config: IStrategyOptions = {
      usernameField: "email",
      passwordField: "password",
    };
    passport.use(
      new LocalStrategy(config, (email, password, done) => {
        if (email === "dev.anhtu@gmail.com" && password === "123123123") {
          return done(null, { id: 4, email: email });
        }
        return done(null, {});
      }),
    );
  };

  initJwtPassport();
  initLocalPassport();
  passport.initialize()(req, res, next);
};

export const jwtGuard = passport.authenticate("jwt", { session: false });
export const localGuard = passport.authenticate("local", { session: false });
