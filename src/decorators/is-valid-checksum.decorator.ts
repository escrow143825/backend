import "reflect-metadata";
import { registerDecorator, ValidationOptions, ValidationArguments, ValidatorConstraint, ValidatorConstraintInterface } from "class-validator";
import crypto from "crypto-js";

@ValidatorConstraint({ name: "isValidChecksum", async: false })
export class IsValidChecksumConstraint implements ValidatorConstraintInterface {
  validate(value: any, args: ValidationArguments) {
    const [fields] = args.constraints;
    const obj = fields.reduce((acc: any, field: string) => {
      acc[field] = (args.object as any)[field];
      return acc;
    }, {} as any);

    const dataString = JSON.stringify(obj);
    const calculatedChecksum = crypto.SHA256(dataString).toString(crypto.enc.Hex);
    return calculatedChecksum === value;
  }

  defaultMessage(args: ValidationArguments) {
    return `Checksum does not match the calculated SHA-256 checksum of the specified fields.`;
  }
}

export function ValidChecksum(fields: string[], validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      constraints: [fields],
      validator: IsValidChecksumConstraint,
    });
  };
}
