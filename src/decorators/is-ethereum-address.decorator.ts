import { ValidatorConstraint, ValidatorConstraintInterface, ValidationArguments, ValidationOptions, registerDecorator } from "class-validator";
import { ethers } from "ethers";

@ValidatorConstraint({ name: "IsEthereumAddress", async: false })
class IsEthereumAddressConstraint implements ValidatorConstraintInterface {
  validate(value: any, args: ValidationArguments) {
    return ethers.isAddress(value);
  }

  defaultMessage(args: ValidationArguments) {
    return "Address ($value) is not a valid Ethereum address!";
  }
}

export function IsEthereumAddress(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: IsEthereumAddressConstraint,
    });
  };
}
