import { TypedContractEvent, TypedEventLog } from "../blockchain/typechain-types/common";
import { TransferEvent } from "../blockchain/typechain-types/contracts/Token";

export class LogEventHanlder {
  private _timestamp: number | null = null;
  private _blockNumber: number;

  constructor(
    private log: TypedEventLog<
      TypedContractEvent<
        TransferEvent.InputTuple,
        TransferEvent.OutputTuple,
        TransferEvent.OutputObject
      >
    >,
  ) {
    this._blockNumber = log.blockNumber;
  }

  async getTimestamp() {
    const block = await this.log.provider.getBlock(this._blockNumber);
    this._timestamp = block?.timestamp || null;
    return this._timestamp;
  }

  get txid() {
    return this.log.transactionHash;
  }

  get from() {
    return this.log.args[0];
  }

  get to() {
    return this.log.args[1];
  }

  get amount() {
    return this.log.args[2];
  }

  get timestamp() {
    return this._timestamp;
  }

  get blockNumber() {
    return this._blockNumber;
  }
}
