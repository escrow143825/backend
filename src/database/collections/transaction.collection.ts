// import { Prisma, TxStatus } from "@prisma/client";
// import { prisma } from "..";

// export class TransactionCollection {
//   async createTx(tx: Prisma.TransactionCreateInput) {
//     return prisma.transaction.create({
//       data: { ...tx },
//     });
//   }

//   async getTransactionsByTrxId(txid: string) {
//     return prisma.transaction.findUnique({ where: { txid: txid } });
//   }

//   async getManyTx(txs: string[]) {
//     return prisma.transaction.findMany({ where: { txid: { in: txs } } });
//   }

//   async createManyTx(txs: Prisma.TransactionCreateManyInput[]) {
//     if (!txs.length) return [];
//     const transactions = await this.getManyTx(txs.map((t) => t.txid));
//     if (!transactions.length) {
//       return await prisma.transaction.createMany({ data: txs });
//     } else {
//       txs = txs.filter((t) => !transactions.map((t) => t.txid).includes(t.txid));
//       return txs.length ? await prisma.transaction.createMany({ data: txs }) : [];
//     }
//   }

//   async updateStatus(txid: string, status: TxStatus) {
//     return prisma.transaction.update({
//       where: { txid: txid },
//       data: { status: status },
//     });
//   }
// }
