import { User } from "@prisma/client";
import { prisma } from "..";
import { NotFound } from "http-errors";
import { Prisma } from "@prisma/client";

export class UserCollection {
  async getUserByWallet(wallet: string): Promise<User>;
  async getUserByWallet(wallet: string, options: { thr: true }): Promise<User>;
  async getUserByWallet(wallet: string, options: { thr: false }): Promise<User | null>;
  async getUserByWallet(wallet: string, options: { thr: boolean } = { thr: true }): Promise<User | null> {
    const user = await prisma.user.findUnique({ where: { wallet } });
    if (options.thr && !user) throw new NotFound("User not found");
    return user;
  }

  createUser(userCreate: Prisma.UserCreateInput) {
    return prisma.user.create({
      data: userCreate,
    });
  }
}
