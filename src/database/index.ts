import { PrismaClient } from "@prisma/client";
import { UserCollection } from "./collections/user.collection";
// import { TransactionCollection } from "./collections/transaction.collection";

export const prisma = new PrismaClient();

export const userCollection = new UserCollection();
// export const transactionCollection = new TransactionCollection();
