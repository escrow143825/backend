import { IsHash, IsIn, IsNotEmpty, IsNumber, IsNumberString, IsOptional, IsString, IsUUID, ValidateNested } from "class-validator";
import { IsEthereumAddress } from "../../../decorators/is-ethereum-address.decorator";
import { ValidChecksum } from "../../../decorators/is-valid-checksum.decorator";
import { NETWORK_SUPPORTS } from "../../../blockchain";
import { NetworkSupport } from "@prisma/client";

export class TrxRequest {
  @IsUUID()
  txId!: string;

  @IsEthereumAddress()
  wallet!: string;

  @IsIn(NETWORK_SUPPORTS)
  network!: NetworkSupport;

  @IsString()
  @IsEthereumAddress()
  token!: string;

  @IsNumber()
  timestamp!: number;

  @IsString()
  @IsNotEmpty()
  @ValidChecksum(["txId", "wallet", "network", "timestamp", "token"])
  checksum!: string;
}

export class TrxResponse {
  @IsIn(["success", "failed"])
  status!: string;

  @IsString()
  @IsNotEmpty()
  message!: string;

  data!: DataResponse;
}

export class DataResponse {
  @IsNumber()
  timestamp!: number;

  @IsIn(NETWORK_SUPPORTS)
  network!: NetworkSupport;

  @ValidateNested()
  transactions!: TrxItem[];
}

export class TrxItem {
  @IsHash("sha256")
  txid!: string;

  @IsEthereumAddress()
  from!: string;

  @IsEthereumAddress()
  to!: string;

  @IsNumberString()
  amount!: string;

  @IsNumber()
  @IsOptional()
  timestamp!: number | null;

  @IsHash("sha256")
  checksum!: string;

  @IsEthereumAddress()
  token!: string;
}

class TransactionRequest {
  @IsHash("256")
  txid!: string;
}
