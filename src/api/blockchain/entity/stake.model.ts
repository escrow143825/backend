import { IsIn, IsNumberString, IsUUID } from "class-validator";
import { IsEthereumAddress } from "../../../decorators/is-ethereum-address.decorator";
import { NETWORK_SUPPORTS } from "../../../blockchain";
import { NetworkSupport } from "@prisma/client";

export class StakeRequest {
  @IsUUID(4)
  id!: string;

  @IsEthereumAddress()
  wallet!: string;

  @IsIn(NETWORK_SUPPORTS)
  network!: NetworkSupport;

  @IsNumberString()
  amount!: string;

  @IsEthereumAddress()
  token!: string;
}
