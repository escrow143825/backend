import { IsBoolean, IsIn, IsNotEmpty } from "class-validator";
import { ValidChecksum } from "../../../decorators/is-valid-checksum.decorator";
import { IsEthereumAddress } from "../../../decorators/is-ethereum-address.decorator";
import { IsPrivateKey } from "../../../decorators/is-private-key.decorator";
import { NETWORK_SUPPORTS } from "../../../blockchain";
import { NetworkSupport } from "@prisma/client";

export class CreateUserRequest {
  @IsNotEmpty()
  @IsEthereumAddress()
  wallet!: string;

  @IsNotEmpty()
  @IsPrivateKey()
  privateKey!: string;

  @IsBoolean()
  isMerchant!: boolean;

  @IsNotEmpty()
  @IsIn(NETWORK_SUPPORTS)
  network!: NetworkSupport;

  @IsNotEmpty()
  @ValidChecksum(["wallet", "privateKey", "isMerchant", "network"])
  checksum!: string;
}
