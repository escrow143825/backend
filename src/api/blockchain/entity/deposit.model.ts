import { IsIn, IsNotEmpty, IsNumber, IsNumberString, IsString, IsUUID } from "class-validator";
import { IsEthereumAddress } from "../../../decorators/is-ethereum-address.decorator";
import { NETWORK_SUPPORTS } from "../../../blockchain";
import { NetworkSupport } from "@prisma/client";
import { ValidChecksum } from "../../../decorators/is-valid-checksum.decorator";

export class DespositRequest {
  @IsUUID("4")
  id!: string;

  @IsEthereumAddress()
  wallet!: string;

  @IsIn(NETWORK_SUPPORTS)
  network!: NetworkSupport;

  @IsEthereumAddress()
  token!: string;

  @IsNumber()
  amount!: number;

  @ValidChecksum(["id", "wallet", "network", "token", "amount"])
  checksum!: string;
}
