import { IsIn, IsNumberString, IsOptional, IsString, IsUUID } from "class-validator";
import { IsEthereumAddress } from "../../../decorators/is-ethereum-address.decorator";
import { NETWORK_SUPPORTS } from "../../../blockchain";
import { NetworkSupport } from "@prisma/client";

export class WithdrawRequest {
  @IsUUID(4)
  id!: string;

  @IsString()
  wallet!: string;

  @IsNumberString()
  amount!: string;

  @IsEthereumAddress()
  token!: string;

  @IsIn(NETWORK_SUPPORTS)
  network!: NetworkSupport;
}
