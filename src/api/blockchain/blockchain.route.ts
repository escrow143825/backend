import { NextFunction, RequestHandler, Router, Request, Response } from "express";
import { createUser, deposit, depositV2, getTransaction, stake, transactions, withdraw } from "./blockchain.controller";

const router = Router();

interface Options {
  statusCode?: number;
  message?: string;
}

const asyncHandler = (fn: (req: Request, res: Response, next: NextFunction) => Promise<any> | any, options?: Options): RequestHandler => {
  return async (req, res, next) => {
    try {
      const result = await fn(req, res, next);
      if (!res.headersSent) {
        if (result instanceof Object) {
          res.status(options?.statusCode || 200).send(result);
        } else {
          res.status(options?.statusCode || 200).send({ result });
        }
      }
    } catch (error) {
      next(error);
    }
  };
};

router.post("/create-user", asyncHandler(createUser, { statusCode: 201 }));
router.post("/transactions", asyncHandler(transactions));
router.post("/deposit", asyncHandler(deposit, { statusCode: 201 }));
router.post("/deposit-v2", depositV2);
router.get("/transaction", asyncHandler(getTransaction));
router.post("/withdraw", asyncHandler(withdraw, { statusCode: 201 }));
router.post("/stake", asyncHandler(stake, { statusCode: 201 }));
router.post("/unstake", asyncHandler(stake, { statusCode: 201 }));

export default router;
