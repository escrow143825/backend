import { AddressLike, ContractTransactionResponse, MaxUint256, Signature, Wallet, randomBytes, uuidV4 } from "ethers";
import { Request, Response } from "express";
import { transformer } from "../../helpers/transformer.helper";
import { CreateUserRequest } from "./entity/create-user.model";
import { Conflict } from "http-errors";
import { DataResponse, TrxItem, TrxRequest, TrxResponse } from "./entity/transaction.model";
import { prisma, userCollection } from "../../database";
import { DespositRequest } from "./entity/deposit.model";
import { DateTime } from "luxon";
import { BadRequest } from "http-errors";
import { LogEventHanlder } from "../../handlers/log-event.handler";
import { plainToInstance } from "class-transformer";
import { contractor } from "../../helpers/contractor.helper";
import { BSC_ESCROW_ADDRESS, INetworkProvider } from "../../blockchain";
import { NotFound } from "http-errors";
import { WithdrawRequest } from "./entity/withdraw.model";
import { StakeRequest } from "./entity/stake.model";
import { BscNetwork } from "../../blockchain/networks/bsc.network";
import { Transaction, TxStatus, User } from "@prisma/client";

export const createUser = async (req: Request, res: Response) => {
  const body = transformer.validateRequestBody<CreateUserRequest>(CreateUserRequest, req.body);
  const network = INetworkProvider.getNetwork(body.network);

  // check if user already exists
  const user = await userCollection.getUserByWallet(body.wallet, { thr: false });
  if (user) throw new Conflict("User already exists");
  const timestamp = (await network.getUser(body.wallet)).timestamp;
  if (timestamp != BigInt(0)) {
    await userCollection.createUser({
      wallet: body.wallet,
      network: body.network,
      privateKey: body.privateKey,
      isMerchant: body.isMerchant,
      timestamp: DateTime.now().toJSDate(),
    });
    throw new Conflict("User already exists");
  }

  // create user
  const txReceipt = await network.createUser(body.wallet, body.isMerchant);
  const userCreate = await prisma.user.create({
    data: {
      wallet: body.wallet,
      network: body.network,
      privateKey: body.privateKey,
      isMerchant: body.isMerchant,
      timestamp: DateTime.now().toJSDate(),
    },
  });

  return {
    // txReceipt,
    user: {
      id: userCreate.id,
      wallet: userCreate.wallet,
      network: userCreate.network,
      isMerchant: userCreate.isMerchant,
      status: "success",
    },
  };
};

export const transactions = async (req: Request, res: Response): Promise<TrxResponse> => {
  const body = transformer.validateRequestBody<TrxRequest>(TrxRequest, req.body);

  // init transaction response
  let response: TrxResponse = new TrxResponse();

  response.status = "success";
  response.message = "Transactions not found";
  response.data = new DataResponse();
  response.data.network = body.network;
  response.data.transactions = [];

  // init network
  const network = INetworkProvider.getNetwork(body.network);
  const user = await userCollection.getUserByWallet(body.wallet);
  const signer = network.getSigner(user.privateKey);

  // get logs
  const logs = await network.getTransferTx(undefined, signer.address, undefined);

  if (!logs.length) {
    return {
      ...response,
      data: { ...response.data, timestamp: DateTime.now().toMillis() },
    };
  }

  const logHandlers = logs.map((log) => new LogEventHanlder(log));
  await Promise.all(logHandlers.map((t) => t.getTimestamp()));

  // save transaction to database
  const txData = logHandlers.map((handler) => {
    return contractor.addCheckSum({
      txid: handler.txid,
      from: handler.from,
      to: handler.to,
      amount: handler.amount.toString(),
      timestamp: handler.timestamp,
      token: body.token,
    });
  });

  // return response
  response.data.timestamp = DateTime.now().toMillis();
  response.data.transactions = plainToInstance(TrxItem, txData);
  return plainToInstance(TrxResponse, response);
};

export const deposit = async (req: Request, res: Response) => {
  const body = transformer.validateRequestBody<DespositRequest>(DespositRequest, req.body);

  const network = INetworkProvider.getNetwork(body.network);
  const user = await network.validateUser(body.wallet);
  const signer = network.getSigner(user.privateKey);

  const allowance = await network.allowance(signer, network.contract);
  if (allowance < body.amount.bigint()) {
    const bnbToApprove = await network.approveEstimateGas(signer, network.contract, MaxUint256);
    const bnbBalanceOfSigner = await network.getBalance(signer);
    if (bnbToApprove > bnbBalanceOfSigner) await network.sendNativeTokenFromAdmin(signer, bnbToApprove - bnbBalanceOfSigner);
    await network.approve(signer, network.contract, MaxUint256);
  }

  const bnbToDeposit = await network.depositEstimateGas(signer, body.amount);
  const bnbBalanceOfSigner = await network.getBalance(signer);
  if (bnbToDeposit > bnbBalanceOfSigner) await network.sendNativeTokenFromAdmin(signer, bnbToDeposit - bnbBalanceOfSigner);

  const depositTx = await network.contract.connect(signer).deposit(body.amount);
  const depositReceipt = await depositTx.wait();
  if (!depositReceipt || depositReceipt.status !== 1) throw new BadRequest("deposit failed");

  return depositReceipt;
};

export const depositV2 = async (req: Request, res: Response) => {
  const body = transformer.validateRequestBody<DespositRequest>(DespositRequest, req.body);
  const transaction = await prisma.transaction.findUnique({ where: { id: body.id } });

  if (transaction && transaction.status === "success") {
    return res.status(200).send({ message: "Deposited", id: body.id });
  }

  const network = INetworkProvider.getNetwork(body.network);
  res.status(200).send({ message: "Depositing...", id: body.id });

  const { id, from, to, amount, token, timestamp } = transaction
    ? {
        id: transaction.id,
        from: transaction.from,
        to: transaction.to,
        amount: transaction.amount,
        token: transaction.token,
        timestamp: transaction.timestamp,
      }
    : {
        id: body.id,
        from: body.wallet,
        to: await network.contractAddress(),
        amount: body.amount.parseEther().toString(),
        token: body.token,
        timestamp: null,
      };

  const upsertTransaction = async (status: TxStatus, timestamp: number | null = null) => {
    await prisma.transaction.upsert({
      where: { id: body.id },
      update: { status },
      create: { id, from, to, amount, token, timestamp, status, type: "deposit" },
    });
  };

  try {
    await upsertTransaction("pending");

    const user = await network.validateUser(from);
    const owner = network.getSigner(user.privateKey);
    const spender = to;
    const allowance = await network.allowance(owner, spender);
    if (allowance < body.amount.parseEther()) network.executePermit(owner, spender);

    const { transactionReceipt } = await network.executeDeposit(owner, body.amount.parseEther());
    const newStatus = transactionReceipt && transactionReceipt.status === 1 ? "success" : "fail";
    upsertTransaction(newStatus, (await transactionReceipt?.getBlock())?.timestamp);
  } catch {
    upsertTransaction("fail");
  }
};

// each 3 seconds call check status transaction
export const getTransaction = async (req: Request, res: Response) => {
  const transaction = await prisma.transaction.findUnique({ where: { id: req.query["id"] as any } });
  if (!transaction) throw new NotFound("Transaction not found");
  return transaction;
};

export const withdraw = async (req: Request, res: Response) => {
  const body = transformer.validateRequestBody<WithdrawRequest>(WithdrawRequest, req.body);
  const transaction = await prisma.transaction.findUnique({ where: { id: body.id } });
  if (transaction && transaction.status === "success") {
    return res.status(200).send({ message: "Withdrew", id: body.id });
  }
  const network = INetworkProvider.getNetwork(body.network);
  const { id, from, to, amount, token, timestamp } = transaction
    ? {
        id: transaction.id,
        from: transaction.from,
        to: transaction.to,
        amount: transaction.amount,
        token: transaction.token,
        timestamp: transaction.timestamp,
      }
    : {
        id: body.id,
        from: body.wallet,
        to: await network.contractAddress(),
        amount: body.amount.parseEther().toString(),
        token: body.token,
        timestamp: null,
      };

  const upsertTransaction = async (status: TxStatus, timestamp: number | null = null) => {
    await prisma.transaction.upsert({
      where: { id: body.id },
      update: { status },
      create: { id, from, to, amount, token, timestamp, status, type: "withdraw" },
    });
  };

  const user = await network.validateUser(from);
  const balanceInContract = (await network.getUser(user.wallet)).balance;
  if (balanceInContract < amount.bigint()) throw new BadRequest("balance is not enough");

  try {
    await upsertTransaction("pending");
    const withdrawReceipt = await network.withdraw(user.wallet, amount);
    const newStatus = withdrawReceipt && withdrawReceipt.status === 1 ? "success" : "fail";
    upsertTransaction(newStatus, (await withdrawReceipt?.getBlock())?.timestamp);
  } catch {
    upsertTransaction("fail");
  }
};

export const stake = async (req: Request, res: Response) => {
  const body = transformer.validateRequestBody<StakeRequest>(StakeRequest, req.body);
  const transaction = await prisma.transaction.findUnique({ where: { id: body.id } });
  if (transaction && transaction.status === "success") {
    return res.status(200).send({ message: "Staked", id: body.id });
  }

  const network = INetworkProvider.getNetwork(body.network);
  const { id, from, to, amount, token, timestamp } = transaction
    ? {
        id: transaction.id,
        from: transaction.from,
        to: transaction.to,
        amount: transaction.amount,
        token: transaction.token,
        timestamp: transaction.timestamp,
      }
    : {
        id: body.id,
        from: body.wallet,
        to: await network.contractAddress(),
        amount: body.amount.parseEther().toString(),
        token: body.token,
        timestamp: null,
      };

  const upsertTransaction = async (status: TxStatus, timestamp: number | null = null) => {
    await prisma.transaction.upsert({
      where: { id: body.id },
      update: { status },
      create: { id, from, to, amount, token, timestamp, status, type: "stake" },
    });
  };

  const user = await network.validateUser(from);
  const balanceInContract = (await network.getUser(user.wallet)).balance;
  if (balanceInContract < amount.bigint()) throw new BadRequest("balance is not enough");

  try {
    await upsertTransaction("pending");
    const stakeReceipt = await network.stake(user.privateKey, amount);
    const newStatus = stakeReceipt && stakeReceipt.status === 1 ? "success" : "fail";
    upsertTransaction(newStatus, (await stakeReceipt?.getBlock())?.timestamp);
  } catch (error) {
    upsertTransaction("fail");
  }
};

export const unstake = async (req: Request, res: Response) => {
  const body = transformer.validateRequestBody<StakeRequest>(StakeRequest, req.body);
  const transaction = await prisma.transaction.findUnique({ where: { id: body.id } });
  if (transaction && transaction.status === "success") {
    return res.status(200).send({ message: "Unstaked", id: body.id });
  }
  const network = INetworkProvider.getNetwork(body.network);
  const { id, from, to, amount, token, timestamp } = transaction
    ? {
        id: transaction.id,
        from: transaction.from,
        to: transaction.to,
        amount: transaction.amount,
        token: transaction.token,
        timestamp: transaction.timestamp,
      }
    : {
        id: body.id,
        from: body.wallet,
        to: await network.contractAddress(),
        amount: body.amount.parseEther().toString(),
        token: body.token,
        timestamp: null,
      };

  const upsertTransaction = async (status: TxStatus, timestamp: number | null = null) => {
    await prisma.transaction.upsert({
      where: { id: body.id },
      update: { status },
      create: { id, from, to, amount, token, timestamp, status, type: "unstake" },
    });
  };

  const user = await network.validateUser(from);
  const balanceStaked = (await network.getUser(user.wallet)).staked;
  if (balanceStaked < amount.bigint()) throw new BadRequest("balance is not enough");

  try {
    await upsertTransaction("pending");
    const stakedReceipt = await network.unstake(user.privateKey, amount);
    const newStatus = stakedReceipt && stakedReceipt.status === 1 ? "success" : "fail";
    upsertTransaction(newStatus, (await stakedReceipt?.getBlock())?.timestamp);
  } catch {
    upsertTransaction("fail");
  }
};
