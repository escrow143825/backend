import { runTypeChain, glob } from "typechain";

async function main() {
  const cwd = process.cwd();

  const paths = {
    root: `${cwd}`,
    artifacts: `${cwd}/src/blockchain/artifacts`,
  };
  const allFiles = glob(cwd, [`${paths.root}/**/artifacts/!(build-info)/**/+([a-zA-Z0-9_]).json`]);
  return await runTypeChain({
    cwd: cwd,
    filesToProcess: allFiles,
    allFiles: allFiles,
    outDir: "src/blockchain/typechain-types",
    target: "ethers-v6",
  });
}

main()
  .then((result) => console.log(result))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
